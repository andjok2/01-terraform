provider "aws" {
  region     = "eu-west-1"
  access_key = var.access_key
  secret_key = var.secret_key
  token = var.session_token
}

variable "session_token" {
  type = string
}

variable "access_key" {
  type = string
}

variable "secret_key" {
  type = string
}

variable "student_name" {
  type    = string
  default = "YOUR_NAME"
  validation {
    condition    = var.student_name != "YOUR_NAME"
    error_message = "Put your real name isntead of YOUR_NAME."
  }
}

variable "ip" {
  type = string
  validation {
    condition     = var.ip != ""
    error_message = "Put your Public IP here (For example: 1.2.3.4/32 ). To find your public ip -> go to google.com and search for 'my ip address'."
  }
}


variable "ssh_public_key" {
  type = string

  validation {
    condition     = substr(trimspace(var.ssh_public_key), 0, 7) == "ssh-rsa"
    error_message = "The ssh_public_key value must be a valid SSH public key."
  }
}

resource "aws_key_pair" "key" {
  key_name   = "${var.student_name}-key-tf-day3"
  public_key = trimspace(var.ssh_public_key)
}

data "aws_security_group" "safe_ports_sg" {
  id = "sg-0c559dad782db1e5e"
}


resource "aws_instance" "instance-01" {
  ami                    = "ami-025e704755b6eac30"
  instance_type          = "t2.micro"
  key_name               = aws_key_pair.key.key_name
  vpc_security_group_ids = [data.aws_security_group.safe_ports_sg.id]

  tags = {
    Name  = "${var.student_name}-BootcampMicroEC2"
    Owner = "${var.student_name}"
  }
}

output "connection_cmd" {
  value = "ssh -l ec2-user -o StrictHostKeyChecking=no ${aws_instance.instance-01.public_ip}"
}
